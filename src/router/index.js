import Vue from "vue";
import Router from "vue-router";
import Hello from "@/components/Hello";
//import ga from 'vue-ga'

Vue.use(Router)




const router = new Router({
    routes: [
        {
            path: '/',
            name: 'Hello',
            component: Hello
        },

    ],
    mode: 'history',

})

//ga(router, 'UA-104503852-1')

export default router
